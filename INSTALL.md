## Installation ##

Install `node`, `npm` and `nodemon`:

```bash
wget https://nodejs.org/dist/v6.10.3/node-v6.10.3-linux-x64.tar.gz -O /tmp/node.tar.gz
tar xvzf /tmp/node.tar.gz --strip-components=1 -C /usr/local
apt-get install npm
npm install -g nodemon

```

Download project archive:

```bash
wget https://gitlab.com/Creased/hooked-on/repository/archive.tar.gz?ref=v0.0.1-a -O /tmp/hooked-on.tar.gz
mkdir /opt/hooked-on/
tar xvzf /tmp/hooked-on.tar.gz --strip-components=1 -C /opt/hooked-on/
pushd /opt/hooked-on/
chmod o+w /opt/hooked-on/logs/

```

Install project:

```bash
npm install

```

Create service for `systemd` to start service at each boot:

```bash
cat <<-'EOF' >/etc/default/hooked-on
##
# Location of project
#
PROJECT_PATH=/opt/hooked-on

##
# General daemon startup options
#
CMD="start"

EOF

cat <<-'EOF' >/etc/systemd/system/hooked-on.service
[Unit]
Description=Simple WEB hooks triggering API

[Service]
User=root
WorkingDirectory=/opt/hooked-on
ExecStart=/usr/bin/npm start
Type=simple

[Install]
WantedBy=basic.target

EOF

systemctl enable hooked-on.service
systemctl start hooked-on.service

mkdir -p /etc/systemd/system/hooked-on.service.d
cat <<-'EOF' >/etc/systemd/system/hooked-on.service.d/exec.conf
[Service]
EnvironmentFile=-/etc/default/hooked-on

# Reset
User=
WorkingDirectory=
ExecStart=

# Define
User=user
WorkingDirectory=${PROJECT_PATH:-/opt/hooked-on}
ExecStart=/usr/bin/npm $CMD

EOF
systemctl --system daemon-reload
systemctl restart hooked-on.service

```

Show status and log of service:

```bash
systemctl status hooked-on.service -l
journalctl -f -u hooked-on.service

```
