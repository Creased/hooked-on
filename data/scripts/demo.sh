#!/bin/bash

##
# Color sets
#

# Reset
RES="\033[0m"

# Regular
YEL="\033[0;33m"

# Bold
BRED="\033[1;31m"
BGRN="\033[1;32m"
BYEL="\033[1;33m"
BMAG="\033[1;35m"
BCYN="\033[1;36m"
BWHI="\033[1;37m"

##
# Colored ASCII Art
#

printf "%b" "`cat <<EOF
\t${YEL}  ____${RES}                             ${BWHI}_ ${RES}
\t${YEL} / ___|${RES}${BMAG}_ __${RES} ${BCYN}___${RES}  ${BRED}__ _${RES} ${BYEL}___${RES}  ${BGRN}___${RES}  ${BWHI}__| |${RES}
\t${YEL}| |${RES}   ${BMAG}| '__${RES}${BCYN}/ _ \U5C${RES}${BRED}/ _\\\`${RES} ${BYEL}/ __|${RES}${BGRN}/ _ \U5C${RES}${BWHI}/ _\\\` |${RES}
\t${YEL}| |___${RES}${BMAG}| |${RES} ${BCYN}|  __|${RES} ${BRED}(_|${RES} ${BYEL}\__ |${RES}  ${BGRN}__|${RES} ${BWHI}(_| |${RES}
\t${YEL} \____${RES}${BMAG}|_|${RES}  ${BCYN}\___|${RES}${BRED}\__,_${RES}${BYEL}|___/${RES}${BGRN}\___|${RES}${BWHI}\__,_|${RES}
\n\t    <--[ Learning is endless ]-->\n\n
EOF`"
